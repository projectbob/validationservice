/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * ValidationService.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.validation.service;


import com.tgetzoya.projectbob.models.LoginModel;
import com.tgetzoya.projectbob.models.RegisterModel;
import com.tgetzoya.projectbob.models.ResponseModel;
import org.springframework.http.ResponseEntity;

/**
 * Interface for validating input.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
public interface ValidationService {
    /**
     * Registers a new user using the input supplied by the end user.
     *
     * @param registerModel the data supplied by the end user
     * @return a UserModel with account information
     */
    ResponseModel validateRegistration(RegisterModel registerModel);

    /**
     * Returns a status of OK if the service is up and running.
     *
     * @return A status of OK if the service is running
     */
    ResponseEntity healthCheck();

    /**
     * Check to see if all the required user login information is given.
     *
     * @param loginModel the user information
     * @return ResponseModel with result information
     */
    ResponseModel validateLogin(LoginModel loginModel);
}
