/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * ValidationInitializer.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.validation.initializer;

import com.tgetzoya.projectbob.services.validation.configuration.ValidationConfiguration;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Spring Initializer for the Validation Service.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
public class ValidationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    /**
     * Gets the root config for this service.
     *
     * @return the class that will serve as the root configuration
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{ValidationConfiguration.class};
    }

    /**
     * Returns null as there is no servelet configuration.
     *
     * @return null
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return null;
    }

    /**
     * Returns the base servelet mapping.
     *
     * @return the base servelet mapping
     */
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
