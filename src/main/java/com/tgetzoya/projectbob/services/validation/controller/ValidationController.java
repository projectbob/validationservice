/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * ValidationController.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.validation.controller;

import com.tgetzoya.projectbob.models.LoginModel;
import com.tgetzoya.projectbob.models.RegisterModel;
import com.tgetzoya.projectbob.models.ResponseModel;
import com.tgetzoya.projectbob.services.validation.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * This controller handles all validation endpoints.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@RestController
public class ValidationController {
    /**
     * The class that will do the actual work.
     */
    @Autowired
    private ValidationService validationService;

    /**
     * Returns the string "OK" if the service is working.
     *
     * @return String "OK" if it's working
     */
    @RequestMapping(value = "/healthCheck",
            method = RequestMethod.GET,
            produces = MediaType.TEXT_XML_VALUE)
    @ResponseBody
    public ResponseEntity healthCheck() {
        return validationService.healthCheck();
    }

    /**
     * Tests to see if all required user input has been supplied.
     *
     * @param registerModel the user input for registering a new user
     * @return the ResponseEntity on whether the required data was supplied
     */
    @RequestMapping(value = "/registration",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity registration(@RequestBody RegisterModel registerModel) {
        ResponseModel response = validationService.validateRegistration(registerModel);
        return ResponseEntity.ok(response);
    }

    /**
     * Tests to see if all required user input has been supplied for logging
     * into the system.
     *
     * @param loginModel the user input for logging in a user
     * @return the ResponseEntity on whether the required data was supplied
     */
    @RequestMapping(value = "/login",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity login(@RequestBody LoginModel loginModel) {
        return ResponseEntity.ok(validationService.validateLogin(loginModel));
    }
}
