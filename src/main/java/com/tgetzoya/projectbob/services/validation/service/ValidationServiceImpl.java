/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * ValidationServiceImpl.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.validation.service;

import com.tgetzoya.projectbob.models.LoginModel;
import com.tgetzoya.projectbob.models.RegisterModel;
import com.tgetzoya.projectbob.models.ResponseModel;
import com.tgetzoya.projectbob.models.error.ErrorModel;
import com.tgetzoya.projectbob.models.error.StandardErrors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation for validating input.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@Component
public class ValidationServiceImpl implements ValidationService {
    /**
     * Checks to see if all the required fields to create a new user has been
     * entered by the user/calling service.
     *
     * @param registerModel the data supplied by the end user
     * @return a ResponseModel with resulting information
     */
    @Override
    public ResponseModel validateRegistration(RegisterModel registerModel) {
        ResponseModel response = new ResponseModel();
        List<ErrorModel> errors = new ArrayList<>();

        if (StringUtils.isBlank(registerModel.getEmail())) {
            errors.add(StandardErrors.EMAIL_MISSING);
        }

        if (null != registerModel.getName()) {
            if (StringUtils.isBlank(registerModel.getName().getFirst())) {
                errors.add(StandardErrors.FIRST_NAME_MISSING);
            }

            if (StringUtils.isBlank(registerModel.getName().getLast())) {
                errors.add(StandardErrors.LAST_NAME_MISSING);
            }
        }

        if (StringUtils.isBlank(registerModel.getPassword())) {
            errors.add(StandardErrors.PASSWORD_MISSING);
        }

        response.setResult(errors.size());
        response.setErrors(errors);

        return response;
    }

    /**
     * Returns a status of OK if the service is running.
     *
     * @return a status of OK if the service is running
     */
    @Override
    public ResponseEntity healthCheck() {
        return ResponseEntity.ok("{\"status\":\"ok\"}");
    }

    /**
     * Check to see if all the required user login information is given.
     *
     * @param loginModel the user information
     * @return ResponseModel with result information
     */
    @Override
    public ResponseModel validateLogin(LoginModel loginModel) {
        ResponseModel response = new ResponseModel();
        List<ErrorModel> errors = new ArrayList<>();

        if (StringUtils.isBlank(loginModel.getEmail())) {
            errors.add(StandardErrors.EMAIL_MISSING);
        }

        if (null == loginModel.getPassword()) {
            errors.add(StandardErrors.PASSWORD_MISSING);
        }

        response.setResult(errors.size());
        response.setErrors(errors);

        return response;
    }
}
