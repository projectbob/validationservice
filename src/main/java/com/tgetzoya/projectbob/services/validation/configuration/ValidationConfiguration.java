/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * ValidationConfiguration.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.validation.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Empty class to serve as the host for the annotations that allow Spring to
 * function as a REST client without the need of an XML configuration file.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.tgetzoya.projectbob.services.validation")
public class ValidationConfiguration {
    /* Empty class.  Defined only for annotations. */
}
